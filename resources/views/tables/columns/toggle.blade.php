@php
    $size = $getSize() ?? 'lg';
    $stateColor = $getStateColor();
    $icon = $getStateIcon();
    $iconClasses = \Illuminate\Support\Arr::toCssClasses([
        match ($stateColor) {
            'danger' => 'text-danger-500',
            'primary' => 'text-primary-500',
            'success' => 'text-success-500',
            'warning' => 'text-warning-500',
            null => \Illuminate\Support\Arr::toCssClasses(['text-gray-700', 'dark:text-gray-200' => config('tables.dark_mode')]),
            default => $stateColor,
        },
        match ($size) {
            'xs' => 'h-3 w-3',
            'sm' => 'h-4 w-4',
            'md' => 'h-5 w-5',
            'lg' => 'h-6 w-6',
            'xl' => 'h-7 w-7',
            default => null,
        },
    ]);
    
@endphp
<div x-data="{ error: undefined, pick: false }"
    {{ $attributes->merge($getExtraAttributes())->class([ "filament-tables-icon-column filament-tables-icon-column-size-{$size}"]) }}
    x-on:mouseover.away=" pick = false">

    <div x-cloak x-ref="panel" class="panel absolute max-w-max top-0 left-0 "
        x-float.placement.right.shift>
        <div class="border border-gray-300 shadow-sm bg-white rounded-xl">
            <div class="p-2">{{ $getLabel() }}</div>
            <div class="flex flex-row flex-nowrap p-2  gap-2  ">
                @foreach ($getPossibleStates() as $_state)
                    <div x-on:click.prevent.stop="
                response = await $wire.updateTableColumnState(@js($getName()), @js($recordKey), @js($_state))
                error = response?.error ?? undefined
                pick=false
            "
                        class=" cursor-pointer flex flex-row"><x-dynamic-component :component="$getIcon($_state)" :class="$getIconClassesForState($_state)" />
                        <span class="{{ $getColorForState($_state) }}">{{ $getLabelForState($_state) }} </span>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
    <div @click="$refs.panel.toggle" class="h-full cursor-pointer">
        <x-dynamic-component :component="$icon" :class="$iconClasses" />
        <div class=" absolute left-0 -bottom-4 z-20 border border-gray-300 shadow-sm bg-white rounded-xl "
            x-show="pick" x-cloak>
            <div class="p-2">{{ $getLabel() }}</div>
            <div class="flex flex-row flex-nowrap p-2  gap-2  ">
                @foreach ($getPossibleStates() as $_state)
                    <div x-on:click.prevent.stop="
                response = await $wire.updateTableColumnState(@js($getName()), @js($recordKey), @js($_state))
                error = response?.error ?? undefined
                pick=false
            "
                        class=" cursor-pointer flex flex-row"><x-dynamic-component :component="$getIcon($_state)"
                            :class="$getIconClassesForState($_state)" />
                        <span class="{{ $getColorForState($_state) }}">{{ $getLabelForState($_state) }} </span>
                    </div>
                @endforeach

            </div>
        </div>
    </div>


</div>
