<?php

namespace KDA\Filament\TernaryIcon;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Filament\PluginServiceProvider;
class TernaryIconServiceProvider extends PluginServiceProvider
{

    protected array $styles = [
        'ternary-toggle-icon-column' => __DIR__ . '/../assets/css/ternary.css',
    ];

    public function configurePackage(Package $package): void
    {
        $package
            ->name('filament-ternary-icon')
            ->hasViews();
    }

    public function packageBooted(): void
    {
        parent::packageBooted();
        //Livewire::component('filament-media-manager-modal',\KDA\Filament\MediaManager\Livewire\MediaItemModal::class);
    }
}
