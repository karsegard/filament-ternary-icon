<?php

namespace KDA\Filament\TernaryIcon\Columns;


use Closure;
use Filament\Tables\Columns\Column;
use Filament\Tables\Columns\Contracts\Editable;
use Filament\Tables\Columns\Concerns;
class TernaryToggleIconColumn extends Column implements Editable
{
  
    use Concerns\CanBeValidated;
    use Concerns\CanUpdateState;
    use Concerns\HasSize;
    use Concerns\HasColors;

    protected string $view = 'filament-ternary-icon::tables.columns.toggle';
    protected string $icon ;
    protected Closure $option_label;

    protected Closure | array $options;

    protected Closure | array $values;

    public function values($values):static
    {
        $this->values = $values;
        return $this;

    }

    public function getPossibleStates():array{
        return $this->evaluate($this->values);
    }
    protected function setUp(): void
    {
        parent::setUp();

        $this->size('lg');
        $this->values([null,0,1]);
        $this->disableClick();
     
        $this->colors([
            'text-gray-400'=>null,
            'text-primary-500'=>1,
            'text-danger-500'=>0
        ]);
    }

    public function options(Closure|array $options){
        $this->options= $options;
        return $this;
    }

    public function getOptions ():array{
        return $this->evaluate($this->options);
    }
   
    public function getStateIcon(): ?string
    {
        $state = $this->getState();
        return $this->getIcon($state);
    }

    public function getIcon($state) : ?string{
        $stateIcon = null;
        $record = $this->getRecord();
        foreach ($this->getOptions() as $icon => $condition) {
            if (is_numeric($icon)) {
                $stateIcon = $condition;
            } elseif ($condition instanceof Closure && $condition($state, $record)) {
                $stateIcon = $icon;
            } elseif ($condition === $state) {
                $stateIcon = $icon;
            }
        }

        return $stateIcon;
    }

    public function icon(string $icon):static
    {
        $this->icon = $icon;
        return $this;
    }


    public function getCurrentIcon(){
        return $this->icon;
    }

    public function getColorForState($state): ?string
    {
        return $this->evaluate($this->color, [
            'state' => $state,
        ]);
    }

    public function getIconClassesForState($state){
        return  \Illuminate\Support\Arr::toCssClasses([
            $this->getColorForState($state),
            $this->getSizeClass()
        ]);
    }

    public function getLabelForState($state){
        return $this->evaluate($this->option_label,['state'=>$state]);
    }

   

    public function optionsLabel(Closure $label){
        $this->option_label = $label;
        return $this;
    }

    

    public function getSizeClass(){
        return  match ($this->getSize()) {
            'xs' => 'h-3 w-3',
            'sm' => 'h-4 w-4',
            'md' => 'h-5 w-5',
            'lg' => 'h-6 w-6',
            'xl' => 'h-7 w-7',
            default => null,
        };
    }
}
